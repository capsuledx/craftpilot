<?php
/**
 * Craftpilot plugin for Craft CMS
 *
 * Craftpilot Variable
 *
 * --snip--
 * Craft allows plugins to provide their own template variables, accessible from the {{ craft }} global variable
 * (e.g. {{ craft.pluginName }}).
 *
 * https://craftcms.com/docs/plugins/variables
 * --snip--
 *
 * @author    Capsule DX
 * @copyright Copyright (c) 2016 Capsule DX
 * @link      https://capsuledx.com
 * @package   Craftpilot
 * @since     0.5
 */

namespace Craft;

class CraftpilotVariable
{
    /**
     * Whatever you want to output to a Twig tempate can go into a Variable method. You can have as many variable
     * functions as you want.  From any Twig template, call it like this:
     *
     *     {{ craft.craftpilot.exampleVariable }}
     *
     * Or, if your variable requires input from Twig:
     *
     *     {{ craft.craftpilot.exampleVariable(twigValue) }}
     */
    public function exampleVariable($optional = null)
    {
        return "And away we go to the Twig template...";
    }
}