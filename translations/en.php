<?php
/**
 * Craftpilot plugin for Craft CMS
 *
 * Craftpilot Translation
 *
 * @author    Capsule DX
 * @copyright Copyright (c) 2016 Capsule DX
 * @link      https://capsuledx.com
 * @package   Craftpilot
 * @since     0.5
 */

return array(
    'Translate me' => 'To this',
);
