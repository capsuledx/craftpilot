<?php
/**
 * Craftpilot plugin for Craft CMS
 *
 * Craftpilot Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Capsule DX
 * @copyright Copyright (c) 2016 Capsule DX
 * @link      https://capsuledx.com
 * @package   Craftpilot
 * @since     0.5
 */

namespace Craft;

class CraftpilotService extends BaseApplicationComponent
{
    /**
     * This function can literally be anything you want, and you can have as many service functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     craft()->craftpilot->exampleService()
     */
    public function exampleService()
    {
    }

}