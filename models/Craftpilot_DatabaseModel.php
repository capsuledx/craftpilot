<?php
/**
 * Craftpilot plugin for Craft CMS
 *
 * Craftpilot_Database Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    Capsule DX
 * @copyright Copyright (c) 2016 Capsule DX
 * @link      https://capsuledx.com
 * @package   Craftpilot
 * @since     0.5
 */

namespace Craft;

class Craftpilot_DatabaseModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'someField'     => array(AttributeType::String, 'default' => 'some value'),
        ));
    }

}