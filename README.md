# Craftpilot plugin for Craft CMS

ServerPilot Access in Craft!

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Craftpilot, follow these steps:

1. Download & unzip the file and place the `craftpilot` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3. Install plugin in the Craft Control Panel under Settings > Plugins
4. The plugin folder should be named `craftpilot` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Craftpilot works on Craft 2.4.x and Craft 2.5.x.

## Craftpilot Overview

-Insert text here-

## Configuring Craftpilot

-Insert text here-

## Using Craftpilot

-Insert text here-

## Craftpilot Roadmap

Some things to do, and ideas for potential features:

* Release it

## Craftpilot Changelog

### 0.5 -- 2016.03.26

* Initial release

Brought to you by [Capsule DX](https://capsuledx.com)